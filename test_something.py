
def test_something(holi):
    assert holi == "holi"


def test_factory(factory):
    assert factory("bla") == "blabla"


def test_parametrized(parametrized):
    assert parametrized in ("hello", "bye")


def test_needsfiles(tmpdir):
    print(tmpdir)
    assert False, "On purpose failed test"
