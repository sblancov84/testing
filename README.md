# Testing

Use fixtures of pytest to know how to work with them. Also I am trying to use gitlab-ci.

# Tools

## Prospector

Prospector is a useful static analysis tool for Python.

It can be configured through its [profile file](https://prospector.readthedocs.io/en/master/profiles.html)

[Profile examples](https://github.com/PyCQA/prospector/tree/master/prospector/profiles/profiles)

## Pytest

[Pytest](https://docs.pytest.org/) is a unit testing tool for Python. It is not only a test runner but also a library and framework to build unit tests.

It use assert keyword as assert library because it makes some kind of magic. [Pytest asserts](https://docs.pytest.org/en/latest/assert.html).

It is compatible with nose and unittest tests.

It has lots of plugins and it accept new ones so it is extensible.

[Full documentation](https://docs.pytest.org/en/latest/contents.html#toc)
