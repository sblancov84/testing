activate:
	pipenv shell

analysis:
	prospector *.py

unit-test:
	pytest test_something.py

see-fixtures:
	pytest --fixtures
