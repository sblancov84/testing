from pytest import fixture


@fixture
def holi():
    return "holi"


@fixture
def factory():
    def something(bla):
        return "bla{}".format(bla)
    return something


@fixture(params=["hello", "bye"])
def parametrized(request):
    return request.param
